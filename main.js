const runCommand = require('./commands/utils/runCommand');
const { cloneRepo } = require('./commands/cloneRepo');
const { installDependencies } = require('./commands/installDependencies');
const { getOutdatedDependencies } = require('./commands/getOutdatedDependencies');
const { updateDependencies } = require('./commands/updateDependencies');
const { commitDependencyChanges } = require('./commands/commitDependencyChanges');
const { pushUpdatesToRepo } = require('./commands/pushUpdatesToRepo');
const { createPullRequest } = require('./commands/createPullRequest');
const { clean } = require('./commands/clean');

async function main() {
  await cloneRepo();
  await installDependencies();
  const dependencies = await getOutdatedDependencies();

  if (!Object.keys(dependencies).length) {
    console.log('nothing to update');
  } else {
    await updateDependencies(dependencies);
    await commitDependencyChanges();
    await pushUpdatesToRepo();
    await createPullRequest();
  }

  await clean();
}

main()
  .then(() => {
    process.exit(0);
  })
  .catch(e => {
    console.error(e.result);
    process.exit(1);
  });
