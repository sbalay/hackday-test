module.exports = {
  CLONED_REPO_FOLDER: '../cloned',
  REPO_ORG: 'sbalay',
  REPO_NAME: 'hackday-test',
  PULL_REQUEST_SOURCE_BRANCH: 'auto-deps-update'
};
