const runCommand = require('./utils/runCommand');
const { CLONED_REPO_FOLDER, PULL_REQUEST_SOURCE_BRANCH } = require('../constants');

module.exports.pushUpdatesToRepo = async function() {
  return runCommand({
    command: [
      'git',
      ['push', 'origin', `HEAD:${PULL_REQUEST_SOURCE_BRANCH}`],
      { cwd: CLONED_REPO_FOLDER }
    ],
    loadingMessage: `Pushing changes to repository in branch: ${PULL_REQUEST_SOURCE_BRANCH}`,
    successMessage: `Branch ${PULL_REQUEST_SOURCE_BRANCH} created`,
    failureMessage: 'Could not push changed to repository'
  });
};
