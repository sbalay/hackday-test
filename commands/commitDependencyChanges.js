const runCommand = require('./utils/runCommand');
const { CLONED_REPO_FOLDER } = require('../constants');

module.exports.commitDependencyChanges = async function() {
  const { spinner } = await runCommand({
    command: ['git', ['add', '.'], { cwd: CLONED_REPO_FOLDER }],
    loadingMessage: 'Commiting version changes'
  });
  try {
    await runCommand({
      command: ['git', ['commit', '-m', 'update dependencies'], { cwd: CLONED_REPO_FOLDER }]
    });
    spinner.succeed('Commit done');
  } catch (e) {
    spinner.fail('Commit failed');
    throw e;
  }
};
