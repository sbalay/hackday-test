const runCommand = require('./utils/runCommand');
const { CLONED_REPO_FOLDER, REPO_ORG, REPO_NAME } = require('../constants');

module.exports.cloneRepo = function() {
  return runCommand({
    command: [
      'git',
      ['clone', `git@bitbucket.org:${REPO_ORG}/${REPO_NAME}.git`, CLONED_REPO_FOLDER]
    ],
    loadingMessage: 'Cloning repository',
    successMessage: 'Repository cloned',
    failureMessage: 'Failed to clone repository'
  });
};
