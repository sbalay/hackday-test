const runCommand = require('./utils/runCommand');
const { CLONED_REPO_FOLDER } = require('../constants');

module.exports.installDependencies = function() {
  return runCommand({
    command: ['npm', ['install'], { cwd: CLONED_REPO_FOLDER }],
    loadingMessage: 'Installing dependencies',
    successMessage: 'Dependencies installed',
    failureMessage: 'Dependencies installation failed'
  });
};
