const runCommand = require('./utils/runCommand');
const { CLONED_REPO_FOLDER } = require('../constants');

module.exports.getOutdatedDependencies = async function() {
  const { result } = await runCommand({
    command: ['npm', ['outdated', '--json', '--long'], { cwd: CLONED_REPO_FOLDER }],
    loadingMessage: 'Looking for outdated dependencies',
    successMessage: 'Dependencies analyzed',
    context: { noFail: true }
  });
  return result ? JSON.parse(result) : {};
};
