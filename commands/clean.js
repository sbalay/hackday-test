const runCommand = require('./utils/runCommand');
const { CLONED_REPO_FOLDER } = require('../constants');

module.exports.clean = function() {
  return runCommand({
    command: ['rm', ['-rf', CLONED_REPO_FOLDER]],
    loadingMessage: 'Removing temp folders',
    successMessage: 'Temp folders removed'
  });
};
