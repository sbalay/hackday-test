const runCommand = require('./utils/runCommand');
const { CLONED_REPO_FOLDER } = require('../constants');

function buildInstallString(deps, dependencies) {
  return deps.map(dep => `${dep}@^${dependencies[dep].latest}`).join(' ');
}

function update(dependenciesString, dev = false) {
  return runCommand({
    command: ['npm', ['install', dependenciesString, '--save'], { cwd: CLONED_REPO_FOLDER }],
    loadingMessage: `Updating outdated ${dev ? 'dev' : 'normal'} dependencies`,
    successMessage: `${dev ? 'Dev' : 'Normal'} dependencies updated`,
    failureMessage: `Updating ${dev ? 'dev' : 'normal'} dependencies failed`
  });
}

module.exports.updateDependencies = async function(dependencies) {
  const normalDeps = Object.keys(dependencies).filter(
    dep => dependencies[dep].type === 'dependencies'
  );
  const devDeps = Object.keys(dependencies).filter(
    dep => dependencies[dep].type === 'devDependencies'
  );

  if (normalDeps.length) {
    await update(buildInstallString(normalDeps, dependencies));
  }

  if (devDeps.length) {
    await update(buildInstallString(devDeps, dependencies), true);
  }
};
