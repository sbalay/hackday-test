# bump it

CLI tool to generate a pull request updating your dependencies to their latest version.

![Alt Text](./assets/bumpit.gif)

## Support

Currently, this tool support:

- Javascript projects using npm as package manager
- Bitbucket.org repositories

## Prerequisites

- node and npm

## Usage

1. Create a file named `credentials.json` and write your bitbucket username and password with the following format

```json
{
  "username": "my-username",
  "password": "my-password"
}
```

The file is ignored from version control, so the credentials will not be uploaded.

2. Modify the file [constants.js](./constants.js) to point the tool to your repository.

Set the organization name using REPO_ORG and repository name using REPO_NAME.
You can also customize the branch name of the pull request source using PULL_REQUEST_SOURCE_BRANCH

3. Run `npm start` and wait for it!
